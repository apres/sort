#Premier tirage au sort pour désigner un.e membre pour s'ajouter au bureau
#Gtaine est la valeur du CAC40 a l'ouverture du marché en Septembre.
import numpy.random as rd #Importer la collection de fonnctions necessaires
graine= float(input('Entrez la graîne:')) #Valeur de la graîne
rd.seed(seed=int(graine)*100) # La graîne determine de façon deterministe le tirage
mbstM = ['L425', 'Erik',
         'Ju'] # Première strate fait des membres hommes (cis/trans)
mbstF = ['Anaideia']

stp1 = [rd.choice(mbstF), rd.choice(mbstM)]  # Choisir un membre de chaque strate
print("On a selectionné ces deux membres pour la première étape: ", stp1)
print("On a désigné pour venir au bureau: ", rd.choice(stp1))
